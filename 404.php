<?php get_header(); ?>

	<div class="post indent">

		<h2>Not found</h2>

		<div class="entry">

			<p>Sorry the page you are looking for couldn't be found. Please use the site menu.</p>

		</div>

	</div>

<?php // get_sidebar(); // Called in header.php ?>

<?php get_footer(); ?>