<aside>
	<div class="container">
				
		<div class="col-lg-3 col-md-3 col-sm-3">
			
			<header>
				<div id="header-cross">
					<a href="<?php bloginfo('url'); ?>/welcome"><img class="alignleft" src="<?php bloginfo('template_url'); ?>/img/cross@half-x.png" data-retina="<?php bloginfo('template_url'); ?>/img/cross.png" width="40" height="53" alt="Anglican Diocese of Auckland Resurrection Calendar" title="Anglican Diocese of Auckland Resurrection Calendar" /></a>
				</div>
				<div id="header-title">
					<a href="<?php bloginfo('url'); ?>/welcome">An<br />Advent<br />Journey<br /><span class="header-date"><?php echo date('Y'); ?></span></a>
				</div>
			</header>
			
			<?php get_template_part("inc/nav"); 
			
			global $ashWed; ?>
			
			<footer>
				<?php /* <div class="social-media-links">
					<p><a href="https://www.facebook.com/resurrection.nz/" target="_blank" title="Resurrection on Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></p>
					<p><a href="https://www.instagram.com/resurrectionnz/" target="_blank" title="Resurrection on Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></p>
					<div class="clear"></div>
				</div> */ ?>
				<a href="http://auckanglican.org.nz" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/Anglican_Diocese_of_Auckland_logo@half-x.png" data-retina="<?php bloginfo('template_url'); ?>/img/Anglican_Diocese_of_Auckland_logo.png" width="133" height="43" alt="Anglican Diocese of Auckland" title="Anglican Diocese of Auckland" /></a>

			</footer>
			
		</div>
		
	</div>
</aside>
