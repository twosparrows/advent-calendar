/* 
Resurrection Calendar

jQuery / Javascript Functionality

(c) 2015 Tim Morgan / Two Sparrows Design www.twosparrows.co.nz
*/


// Global Variables


// Functions

var waitForFinalEvent = (function () { // see http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

function tspUnlockAside() {
	if ( jQuery( '#aside-wrap' ).height() > $(window).height()) { // 80px for margins on aside header and footer
		jQuery('#aside-wrap').addClass('unlock');//
	} else {
		jQuery('#aside-wrap').removeClass('unlock');
	}
}


// Document Ready

jQuery(document).ready(function($){

   	// Swap out images using data-retina attribute with retina images (mostly footer images)
	if (window.devicePixelRatio >= 1.5) {
   		$('img').each(function(i){
   			var retina = $(this).attr('data-retina');
   			var _this = $(this); // Assign to a variable so will execute after AJAX if this is reassigned
   			if ((typeof retina != "undefined") && (retina != null) && (retina != "") && (retina != 0)) {
				$.get(retina).done(function(){ 
					_this.attr('src', retina);
				});
   			}
   		});
	} else if ((jQuery(window).width() < 991) && (jQuery(window).width() > 628)) { // SM size fix - get the retina size image as the box is double width. The rest of the image resizing is handled by CSS. This just improves the quality.
		jQuery('.lent-friday .lent-day-image img').each(function(i){
   			var retina = $(this).attr('data-retina');
   			var _this = $(this); // Assign to a variable so will execute after AJAX if this is reassigned
   			if ((typeof retina != "undefined") && (retina != null) && (retina != "") && (retina != 0)) {
				$.get(retina).done(function(){ 
					_this.attr('src', retina);
				});
   			}
   		});
	}
	

	// If viewable screen height is less than the height of the individual elements of the aside, then hide fixed aside (displaying unfixed aside underneath
	tspUnlockAside();
	jQuery(window).resize(function () {
	    waitForFinalEvent(function(){
			tspUnlockAside();
	    }, 500, "UnlockAside");
	});

	
	// Lent Day Hover
	if (jQuery("body.single").length == 0) { // Don't action on Single pages
		$('div.lent-day-surround').hover(function(){ // Adds a cursor pointer only
			$(this).addClass('hovering');
		}, function(){
			$(this).removeClass('hovering');
		});
	}
		
	// Lent Day Click
	if (jQuery("body.single").length == 0) { // Don't action on Single pages
		$('div.lent-day-surround').click(function(){
			$('.open').not($(this).children('.lent-day')).removeClass('open');
			$(this).children('.lent-day').toggleClass("open");
		});
	}
});