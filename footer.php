	<?php if (!is_page_template("page-splash.php")) { ?>
				<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>
			</div><?php // End embedded row ?>
		</div>
	
		<?php // <div class="col-lg-0"></div><?php // Right spacer ?>
		
	</div>
	
	<?php }

	// Bootstrap Size Test
	if (function_exists("tsp")) {
		if (tsp()) { 
			if (isset($_GET['bs'])) {
				if ($_GET['bs'] == "true") { ?>
					<div id="bootstrap-test" class="container-fluid clearfix">
						<div class="row">
							<div class="visible-lg"><p>Bootstrap LG size active</p></div>
							<div class="visible-md"><p>Bootstrap MD size active</p></div>
							<div class="visible-sm"><p>Bootstrap SM size active</p></div>
							<div class="visible-xs"><p>Bootstrap XS size active</p></div>
						</div>
					</div>
				<?php }
			}
		}
	}
		
	wp_footer(); 
	
	// Don't forget analytics ?>

</body>
</html>