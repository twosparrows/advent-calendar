<?php
/*
	Template Name: Splash Page
*/


/*
	NOTES ABOUT THIS SITE:
	
	- This site is built on the framework of the Lent Calendar, and then the Resurrection Calendar - hence there are still references to both throughout
	- The main class is created in inc/resurrection.php, which sets up all the dates etc
	- Because of the underlying frameworks from the Lent and Resurrection Calendars, the start date in the Resurrection class is known as $easterSunday, and the end date is known as $pentecostSunday
	- All dates are automatically calculated from the first Sunday in Advent. These dates are set up in the database until 2030 (see top of inc/resurrection.php file)
*/



get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); 
		
		if (has_post_thumbnail()) { ?>
			<style type="text/css">
				html.splash {
					background: url('<?php the_post_thumbnail_url("splash"); ?>') no-repeat center center fixed; 
					-webkit-background-size: cover;
					-webkit-backface-visibility: hidden;
					-moz-background-size: cover;
					-o-background-size: cover;
					background-size: cover;
				}

				@media (min-width: 1200px) {
					html.splash {
						background: url('<?php the_post_thumbnail_url("splash-retina"); ?>') no-repeat center center fixed; 
						-webkit-background-size: cover;
						-moz-background-size: cover;
						-o-background-size: cover;
						background-size: cover;
					}
				}
			</style>
		<?php }	?>
			
		<div class="post" id="post-<?php the_ID(); ?>">

			<div id="splash-intro-container" class="container-fluid clearfix">
				<div class="row clearfix">
							
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			
						<div id="splash-intro-outer"><?php // For horizontal centering ?>
							<section id="splash-intro">
								<div id="splash-intro-inner" class="entry"><?php // For vertical centering ?>
									<a href="<?php echo home_url('welcome'); ?>"><div class="white-cross"></div></a>
									<?php the_content(); ?>
								</div>
							</section>
						</div>
			
					</div>    
				</div>
			</div>

		</div>
		
	<?php endwhile; endif; ?>

<?php get_footer(); ?>