<?php


/*
	NOTES ABOUT THIS SITE:
	
	- This site is built on the framework of the Lent Calendar, and then the Resurrection Calendar - hence there are still references to both throughout
	- The main class is created in inc/resurrection.php, which sets up all the dates etc
	- Because of the underlying frameworks from the Lent and Resurrection Calendars, the start date in the Resurrection class is known as $easterSunday, and the end date is known as $pentecostSunday
	- All dates are automatically calculated from the first Sunday in Advent. These dates are set up in the database until 2030 (see top of inc/resurrection.php file)
*/



// Set Timezone to New Zealand
$serverTZ = new DateTimeZone(date('e'));
date_default_timezone_set('Pacific/Auckland'); // See http://borishoekmeijer.nl/resolve-current-date-and-time-timezone-issue-in-wordpress/


// Include Resurrection Class
include( get_template_directory() . '/inc/resurrection.php' );



// Close Comments and Pings by default
if (get_option('default_comment_status') !== 'closed' || get_option('default_ping_status') !== 'closed') { 
	update_option( 'default_comment_status', 'closed' );
	update_option( 'default_ping_status', 'closed' );
}

// Add RSS links to <head> section
automatic_feed_links();
	
// Load Scripts
function tsp_load_scripts() {	
	// jQuery & other Dependencies
  	wp_deregister_script('jquery');
   	wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"), false);
   	wp_enqueue_script('jquery');
   	wp_register_script('bootstrap', ("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"), array('jquery'), '3.3.5', false);
   	wp_enqueue_script('bootstrap');

   	/* wp_register_script('hoverIntent', get_bloginfo('template_url') . '/js/hoverIntent.js', array('jquery'), '1.0', false);
   	wp_enqueue_script('hoverIntent'); */

	// Load Site Script
	wp_register_script('lent', get_bloginfo('template_url') . '/js/lent.js', array('jquery', 'hoverIntent'), '1.0', false);
	wp_enqueue_script('lent');
}
if ( !is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'tsp_load_scripts' );
}

// is_local - checks if using localhost or not
function is_local() {
	if (substr(get_bloginfo("url"), 0, 22) === "http://localhost:8888/")
		return true;
	else
		return false;
}

// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// Declare sidebar widget zone
if (function_exists('register_sidebar')) {
	register_sidebar(array(
		'name' => 'Sidebar Widgets',
		'id'   => 'sidebar-widgets',
		'description'   => 'These are widgets for the sidebar.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}


// Register Menus
function register_menu() {
  	register_nav_menus(
	    array(
	      'main-menu' => __( 'Main Menu' ),
		)
	);
}
add_action( 'init', 'register_menu' );	

// Images
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
}
if (function_exists('add_image_size')) {
	add_image_size('lent-day', 300, 300, true);
	add_image_size('lent-day-retina', 600, 600, true);
	add_image_size('lent-day-double', 600, 300, true);
	add_image_size('lent-day-double-retina', 1200, 600, true);
	add_image_size('lent-day-triple', 900, 300, true);
	add_image_size('lent-day-triple-retina', 1800, 600, true);
	add_image_size('splash', 972, 648, true);
	add_image_size('splash-retina', 1944, 1296, true);
	add_image_size('single', 600, 600, true);
	add_image_size('single-retina', 1200, 1200, true);
	/*add_image_size('lent-rest-day-special', 600, 300, true);
	add_image_size('lent-rest-day-special-retina', 920, 600, true);*/
}

// Add Attachment ID column to Media Library - see http://wpsnipp.com/index.php/functions-php/media-library-admin-columns-with-attachment-id/
function posts_columns_attachment_id($defaults){
    $defaults['wps_post_attachments_id'] = __('ID');
    return $defaults;
}
function posts_custom_columns_attachment_id($column_name, $id){
        if($column_name === 'wps_post_attachments_id'){
        echo $id;
    }
}
add_filter('manage_media_columns', 'posts_columns_attachment_id', 1);
add_action('manage_media_custom_column', 'posts_custom_columns_attachment_id', 1, 2);

// Get Attachment Details - from http://wordpress.org/ideas/topic/functions-to-get-an-attachments-caption-title-alt-description
function wp_get_attachment_details( $attachment_id ) {

	$attachment = get_post( $attachment_id );
	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}


// Get Page title as a variable suitable for use in CSS
function tsp_page_title_variable($title) {
	$return = strtolower($title);
    $return = preg_replace("/&#?[a-z0-9]{2,8};/i","", $return); // Remove any HTML entities
    $return = preg_replace("/[^a-z0-9_\s-]/", "", $return); // Make alphanumeric (removes all other characters) - see http://stackoverflow.com/questions/11330480/strip-php-variable-replace-white-spaces-with-dashes
    $return = preg_replace("/[\s-]+/", " ", $return); // Clean up multiple dashes or whitespaces
    $return = preg_replace("/[\s_]/", "-", $return); // Convert whitespaces and underscore to dash
    return $return;
}

// Get Page title and return as a string to use on the body element as an ID
function tsp_get_body_id() {
	$return = tsp_page_title_variable(get_the_title());
    // Create string
    $return = ' id="' . $return . '-page" ';
	return $return;
}


// Is anyone from Two Sparrows Logged in? For Data Testing
if (!(function_exists("is_tsp"))) {
	function is_tsp($data = "") {
		if (is_user_logged_in()) {
			global $current_user;
			get_currentuserinfo();
			if (($current_user->user_email == "tim@twosparrows.co.nz") && (($current_user->user_firstname == "Tim") || ($current_user->user_firstname == "Two")) || ($current_user->user_email == "*@twosparrows.co.nz") && ($current_user->user_firstname == "*")) {
				if ($data != "") {
					echo '<p style="color: #f00;">';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</p>';
				}
				return true;
			}
		}
		return false;
	}
}

if (!(function_exists("is_tim"))) {
	function is_tim($data="") { // Old version of the above
		return is_tsp($data);
	}
}

if (!(function_exists("tsp"))) {
	function tsp($data="") { // Old version of the above
		return is_tsp($data);
	}
}

// Displays this Year
function tsp_echo_year() {
	if (date("Y") < 2017) { // 2017 is the minimum (first) year
		return 2017;
	} else {
		return date('Y');
	}
}
add_shortcode('this_year', 'tsp_echo_year'); // Use remove_shortcode to remove it, or remove_all_shortcodes if needed

// Add a link as a button
function tsp_create_button($atts) {

	// Extract Attributes
	extract( shortcode_atts( array(
		'text' => '',
		'link' => '',
		'class' => '', // Any additional classes required for styling
		), $atts )
	);
	
	if ($text != "") { // Can't create a button with no text in it!
		
		// Link setup (if required) and sanitising
		if ($link == "") { // If no link is specified, then the link will default to the welcome page, not the Splash page) 
			$link = get_permalink(get_page_by_title("welcome"));
		} elseif (substr($link, 0, 4) !== "http" && substr($link, 0, 7) !== "mailto:") { // Automatically adds home url to localised link if full link not specified
			$link = get_bloginfo("url") . "/" . $link;
			
		}
		$link = esc_url($link); // sanitised url
			
		// Setup & Return
		$return = '<a class="button';
		if ($class != "")
			$return .= ' ' . $class;
		$return .= '" href="' . $link . '">' . $text . '</a>';
		return $return;
	}
}
add_shortcode('button', 'tsp_create_button'); // Use remove_shortcode to remove it, or remove_all_shortcodes if needed

// Is staff Logged in? For Data Testing
function is_staff($data="") { // Same as is_tim() but for all administrators. Displays in a more website friendly format.
	if (is_user_logged_in()) {
		if (current_user_can('manage_options')) {
			if ($data != "") {
				echo '<p><strong><em>';
				if ((is_array($data)) || (is_object($data))) {
					print_r($data);
				} else {
					echo $data;
				}
				echo '</em></strong></p>';
			}
			return true;
		}
	}
	return false;
}

// Change all WP Querys to order alphabetically by post title (see https://codex.wordpress.org/Alphabetizing_Posts under Main Page)
function tsp_modify_query_order( $query ) {
    if ($query->is_main_query()) {
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );
    }
}
add_action( 'pre_get_posts', 'tsp_modify_query_order' );


// Sets the minimum and maximum thresholds of a number
function set_thresholds($number, $min, $max) {
	if ($number < $min) {
		$number = $min;
	} elseif ($number > $max) {
		$number = $max;
	}
	return $number;
}

?>