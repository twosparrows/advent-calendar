<?php


/*
	NOTES ABOUT THIS SITE:
	
	- This site is built on the framework of the Lent Calendar, and then the Resurrection Calendar - hence there are still references to both throughout
	- The main class is created in inc/resurrection.php, which sets up all the dates etc
	- Because of the underlying frameworks from the Lent and Resurrection Calendars, the start date in the Resurrection class is known as $easterSunday, and the end date is known as $pentecostSunday
	- All dates are automatically calculated from the first Sunday in Advent. These dates are set up in the database until 2030 (see top of inc/resurrection.php file)
*/



$updatedTimestamp = 1542007211;
	
?><!DOCTYPE html>
<html <?php if (is_page_template("page-splash.php")) { ?>class="splash" <?php } language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<?php // Bootstrap CSS ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	
	<?php // Fonts ?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fanwood+Text" type="text/css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Arvo" type="text/css">
	<?php // Load Proxima Nova font using Adobe Typekit ?>
	<script src="https://use.typekit.net/cfa3ocb.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
	
	<?php // CSS ?>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>?ts=<?php echo $updatedTimestamp; ?>">
		
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php echo tsp_get_body_id(); body_class(); ?>>

<?php get_template_part("inc/analytics-tracking"); ?>
	
<?php if (is_page_template("page-splash.php")) { ?>

	<div id="header-splash" class="container-fluid clearfix">
		<div class="row clearfix">
			
			<div class="col-lg-0 col-md-0 col-sm-0 col-xs-0"></div>
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	
				<header class="splash clearfix">
					<div id="header-logo">
						<a href="//auckanglican.org.nz" target="_blank"><img src="<?php bloginfo('template_url'); ?>/img/Anglican_Diocese_of_Auckland_logo@half-x.png" data-retina="<?php bloginfo('template_url'); ?>/img/Anglican_Diocese_of_Auckland_logo.png" width="133" height="43" alt="Anglican Diocese of Auckland" title="Anglican Diocese of Auckland" /></a>
					</div>
				</header>
	
			</div>    
		</div>
	</div>
	
<?php } else { // Normal pages - use aside.php instead ?>

	<div id="aside-wrap" class="container-fluid" style="position: fixed;"><?php // Have used inline CSS here because it wasn't being picked up at all in the CSS file ?>
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<?php get_sidebar(); // This one can be hidden if the browser window is not high enough ?>
		</div>
	</div>
	
	<div id="all-wrap" class="container-fluid">
		
		<div id="fixed-aside" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<?php get_sidebar(); // This one is behind and will operate with scroll ?>
		</div>
	
		<div id="main-content" class="<?php if (is_category()) { echo 'lent-category '; } ?>clearfix col-lg-9 col-md-9 col-sm-9 col-xs-12">
			
			<div class="row clearfix">

<?php } ?>
