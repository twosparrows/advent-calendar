<?php


/*
	NOTES ABOUT THIS SITE:
	
	- This site is built on the framework of the Lent Calendar, and then the Resurrection Calendar - hence there are still references to both throughout
	- The main class is created in inc/resurrection.php, which sets up all the dates etc
	- Because of the underlying frameworks from the Lent and Resurrection Calendars, the start date in the Resurrection class is known as $easterSunday, and the end date is known as $pentecostSunday
	- All dates are automatically calculated from the first Sunday in Advent. These dates are set up in the database until 2030 (see top of inc/resurrection.php file)
*/



get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<div class="post indent" id="post-<?php the_ID(); ?>">

			<h2><?php the_title(); ?></h2>

			<div class="entry">

				<?php the_content(); ?>

			</div>

		</div>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>