<nav class="navbar navbar-default" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse clearfix">
			<?php
			
			// Get Ash Wednesday
			global $resurrection, $resCalWeeks;

			if ($resCalWeeks != "") {
				$lentWeeks = $resCalWeeks; // lent_get_lent_week_dates($ashWed["timestamp"], 40, time());
			} else {
				$lentWeeks = $resurrection->getWeekDates();
			}
			$thisLentWeek = lent_lent_number_extract(single_cat_title('', false));
			
			// Create Menu - based on https://codex.wordpress.org/Function_Reference/wp_get_nav_menu_items#Building_simple_menu_list
		    $menuName = 'main-menu';
			$menuList = '<ul class="nav navbar-nav pull-right">';
			
		
		    if (($locations = get_nav_menu_locations()) && (isset($locations[$menuName]))) {
				
				// Use slug to get menu ID and then get all menu items
				$menu = wp_get_nav_menu_object($locations[$menuName]);
				$menuItems = wp_get_nav_menu_items($menu->term_id);
								
				foreach ( (array) $menuItems as $key => $menuItem ) {
					
					$week = preg_replace("/[^0-9]/", "", $menuItem->title);
					$menuList .= '<li';
					if ((is_category() && $thisLentWeek == $week) || (trim($menuItem->title) == trim(wp_title('', false)))) {
						$menuList .= ' class="current"';
					}
					$menuList .= '><a href="' . $menuItem->url . '"';					
					if ($menuItem->target == "_blank") {
						$menuList .= ' target="_blank"';
					}
					$menuList .= '>' . $menuItem->title;
					if (strpos($menuItem->title, "Week ") !== false) {
						$menuList .= '<br/><span class="date">' . $lentWeeks['week' . $week . 'display_short'] . '</span>';
					}
					$menuList .= '</a></li>';

				} // End foreach

			} // End if menu is set
			
			// If last item was a child menu item, then the child menu needs to be closed before the main menu can be closed (same as above circa line 80)
			if ($isChildMenuItem == true) {
				$menuList .= '</ul></li>';
			}
			
			// Close main menu ul and display
			$menuList .= '</ul>';
		    echo $menuList; ?>
		    
		</div><?php // End .nav-collapse ?>
	</div>
</nav>