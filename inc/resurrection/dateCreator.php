<?php
	
// Resurrection Date Creator

// (c) 2016 by Tim Morgan / Two Sparrows

// DO NOT use this file! It creates multiples of each post for some reason!

// Only include this file once, and only when required! Do NOT run this file twice!

// Uncomment the wp_insert_post line to make it work - commented out here so that it doesn't inadvertantly get run twice.


// Set up
$creatorData = new calendar();
$startFromDayNumber = 27; // Not zero indexed

$weekOffset = $startFromDayNumber % 7;

// Functions

// Is anyone from Two Sparrows Logged in? For Data Testing
if (!(function_exists("tsp"))) {
	function tsp($data = "") {
		if (is_user_logged_in()) {
			global $current_user;
			get_currentuserinfo();
			if (($current_user->user_email == "tim@twosparrows.co.nz") && (($current_user->user_firstname == "Tim") || ($current_user->user_firstname == "Two")) || ($current_user->user_email == "*@twosparrows.co.nz") && ($current_user->user_firstname == "*")) {
				if ($data != "") {
					echo '<p style="color: #f00;">';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</p>';
				}
				return true;
			}
		}
		return false;
	}
}


// Execution

// if (!post_exists("Day 57 (Sunday)", "")) {
	tsp("Creating dates....");
	
	for ($i=$startFromDayNumber; $i<($creatorData->numberOfDays+1); $i++) {
		
		$daysOfTheWeek = array(
			0 => "Saturday",
			1 => "Sunday",
			2 => "Monday",
			3 => "Tuesday",
			4 => "Wednesday",
			5 => "Thursday",
			6 => "Friday",
			7 => "Saturday",
		);
		$dayIndex = $i % 7;
		$week = ceil($i / 7);
		if ($week > 8) {
			$week = 8;
		}
		$catID = get_cat_ID("Week " . $week);
		tsp("Day " . $i . ": " . $daysOfTheWeek[$dayIndex] ." (Week " . $week . ")");
		$args = array(
			'post_title' => "Day " . $i . " (" . $daysOfTheWeek[$dayIndex] . ")",
			'post_status' => "publish",
			'post_category' =>array($catID),
			'post_author' => 1,
			'post_content' => "<strong>" . $daysOfTheWeek[$dayIndex] . "</strong>",
		);
		
		// Create Post
		// wp_insert_post($args, true);
		
	}
	tsp("Dates created!");
/*} else {
	tsp("Dates already created.");
}*/

?>