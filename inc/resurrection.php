<?php 
	
// Set up Easter Sundays in Database (from 2017 to 2050), then set up $easterSunday fot this year
if (get_option('firstSundayInAdvent2018') !== '20181202' || get_option('firstSundayInAdvent2030') !== '20500410') { // Easter Sunday dates taken from http://www.timeanddate.com/holidays/us/easter-sunday
	update_option('firstSundayInAdvent2018','20181202');
	update_option('firstSundayInAdvent2019','20191201');
	update_option('firstSundayInAdvent2020','20201129');
	update_option('firstSundayInAdvent2021','20211128');
	update_option('firstSundayInAdvent2022','20221127');
	update_option('firstSundayInAdvent2023','20231203');
	update_option('firstSundayInAdvent2024','20241201');
	update_option('firstSundayInAdvent2025','20251130');
	update_option('firstSundayInAdvent2026','20261129');
	update_option('firstSundayInAdvent2027','20271128');
	update_option('firstSundayInAdvent2028','20281203');
	update_option('firstSundayInAdvent2029','20291202');
	update_option('firstSundayInAdvent2030','20301201');	

}

// Set Easter Sunday (set here so is global across the theme)
$maxNumberOfWeeks = 8; // The maximum number of Lent weeks. Normally this will be 7


// Automatically create Date posts - NB: ONLY CALL ONCE!
// get_template_part("inc/resurrection/dateCreator");


// Set up Calendar Class
class calendar {
	
	// Class Properties (Variable set up)
	// User defined properties
	public $introDays = 0; // Number of days before Easter Sunday
	public $daysPentecostAfterEasterSunday = 0; // Number of days from Easter Sunday (first day of calendar) to Pentecost Sunday (last day of calendar)
	
	// Auto-defined Properties (most just initiated here, correct values set up within the class construct) 
	public $easterSunday = 0;
	public $pentecostSunday = 0;
	public $today = 0;
	public $daySeconds = 0; // The length of a day in seconds
	public $setDate = false; // Required for the sidebar settings when in test mode
	public $startDate = 0;
	public $endDate = 0;
	public $overcomeDaylightSavingIssuesBySeconds = 0; // Don't add two hours (in seconds) to Easter Sunday timestamp - helps with getting over any daylight saving issues
	public $startOfTheWeekNumeric = 0;
	public $specialDays = array();
	public $specialDayNumbers = array();
	public $specialDaysSorted = array(); // Alt version of specialDays where the keys are the special day numbers
	public $numberOfDays = 0;
	public $numberOfWeeks = 0;
	public $todayDayNumber = 0;
	public $year = 0;
	public $testMode = false; // TEST MODE SET TO TRUE TRUE WILL MAKE ALL WEEKS VISIBLE (but only if Admin user is logged in)
	public $visibleDaysInAdvance = 3; // How many days in advance each week becomes visible
	public $visibleDaysInAdvanceDescription = "three days"; // Used in the text of the website
	private $i; // Used for indexes
	
	public function __construct() {
		
		// Initial value calculations
		$this->daySeconds = (60*60)*24; // The length of a day in seconds
		
		// Set up Date information
		if (date("Y") < 2018) { // Set minimum year to 2018
			$this->year = 2018;
		} else {
			$this->year = date("Y");
		}
		$this->easterSunday = $this->dateSetup(get_option('firstSundayInAdvent' . $this->year)); // Easter Sundays are preset up until 2030
		$this->easterSunday['timestamp'] += $this->overcomeDaylightSavingIssuesBySeconds; // Add two hours (in seconds) - helps with getting over any daylight saving issues
		$this->today = $this->dateSetup();
		$this->pentecostSunday = $this->dateSetup($this->year . '1225'); // Set to Christmas Day - this becomes the END DATE
		$this->startDate = $this->dateSetup($this->easterSunday["timestamp"] - ($this->introDays * $this->daySeconds), true);
		$this->endDate = $this->pentecostSunday;
		$this->startOfTheWeekNumeric = date("N", $this->startDate["timestamp"]); // 1 (for Monday) through 7 (for Sunday)
		$this->daysPentecostAfterEasterSunday = $this->dateSubtractor($this->pentecostSunday["8digit"], $this->easterSunday["8digit"]);
		if (is_staff()) {
			if (isset($_GET['setdate'])) {
				$this->today = $this->dateSetup($_GET['setdate']); // setdate must be supplied in 8digit format
				$this->setdate = $this->today; // Required for the sidebar settings when in test mode
			}
		}
		
		// Set up Special Days (set from 8digit value)
		// Static Dates (these will be overridden by special relative days if there are any conflicts)
		$this->specialDays = array(
			array(
				"name" => "Christmas Eve",
				"8digit" => date('Y') . "1224",
			),
			array(
				"name" => "Christmas Day",
				"8digit" => date('Y') . "1225",
			)			
		);
		for ($i=0; $i<count($this->specialDays); $i++) {
			$this->specialDays[$i]["dayNumber"] = $this->getDayNumber($this->specialDays[$i]["8digit"]);
			if ($this->specialDays[$i]["dayNumber"] > 0) {
				$this->specialDays[$i]["relativeDayNumber"] = $this->specialDays[$i]["dayNumber"] - $this->introDays;
			} else {
				$this->specialDays[$i]["relativeDayNumber"] = "";
			}
		}
		// Relative Dates (set from relative day number (relative to reference day, in this case Easter Sunday)
		/*$this->specialDays = array_merge($this->specialDays, array(
			array(
				"name" => "Palm Sunday",
				"relativeDayNumber" => -7,
			),
			array(
				"name" => "Easter Day",
				"relativeDayNumber" => 0,
			),
		));*/
		// Add Day numbers and 8 digit day values to array
		for ($i=0; $i<count($this->specialDays); $i++) {
			if (!isset($this->specialDays[$i]["8digit"])) { // Static days already set, so ignored here
				$this->specialDays[$i]["dayNumber"] = $this->specialDays[$i]["relativeDayNumber"] + $this->introDays + 1;
				$this->specialDays[$i]["8digit"] = $this->dateAdder($this->startDate["8digit"], ($this->specialDays[$i]["dayNumber"] - 1));
			}
			array_push($this->specialDayNumbers, $this->specialDays[$i]["dayNumber"]);
		}
		sort($this->specialDayNumbers);
		// Set up specialDaysSorted
		for ($i=0; $i<count($this->specialDays); $i++) {
			$newKey = $this->specialDays[$i]["dayNumber"];
			$newSpecialDay = array(
				"name" => $this->specialDays[$i]["name"],
				"relativeDayNumber" => $this->specialDays[$i]["relativeDayNumber"],
                "8digit" => $this->specialDays[$i]["8digit"]
            );
            $this->specialDaysSorted[$newKey] = $newSpecialDay; // Don't sort as it changes the keys!
		}
		
		// Set up Day number information
		$this->numberOfDays = $this->dateSubtractor($this->endDate["8digit"], $this->startDate["8digit"]) + 1; // + 1 so that it is inclusive
		$this->numberOfWeeks = ceil($this->numberOfDays / 7);
		$this->todayDayNumber = $this->getDayNumber();
	}
	


	// Class Methods (Functions)
	public function dateSetup($timeAndDate = "unspecified", $timestamp=false) { // Take any date and time format and return it as an array with both words and numbers for the date. Date should be in the format "5pm 12/2/2014", "10:30am 20151212" or "09:15 12/12/14" or a combination of these. Time must come before date!
	
	
		if ($timestamp == true) { // Supplied in timestamp format
			
				$return = array(
					'timestamp' => $timeAndDate,
					'8digit' => date("Ymd", $timeAndDate),
					'year' => date("Y", $timeAndDate),
					'year_short' => date("y", $timeAndDate),
					'month' => date("n", $timeAndDate),
					'day' => date("j", $timeAndDate), 
					"date_supplied" => false,
					"error" => false,
				);
				$time = "**supplied**"; // Placeholder to force creation below
			
		} else { // Supplied in 8 digit format

			if ($timeAndDate == "unspecified") {
				$now = strtotime('now');
				$timeAndDate = date('g:ia j/n/Y', $now);
			}
			
			// Set up Variables
			$timeAndDate = trim($timeAndDate); // Remove any white space from the start or end
			$timeAndDate = preg_replace("/ {2,}/", " ", $timeAndDate); // Replace multiple spaces with a single space (so that the arrays indexes remain correct once exploded). See http://stackoverflow.com/questions/22192324/replace-multiple-spaces-in-a-string-with-a-single-space
			$return = array();
			$return["error"] = false;
			$return["time_supplied"] = false; // Default
			
			// Check for Time
			$time_date_split = explode(" ", $timeAndDate); // Must be separated by " " space character
			if ($time_date_split[0] == $timeAndDate) { // No time was present, so let's set the time to empty
				$time = "";
				$date = $timeAndDate;
			} else {
				$time = $time_date_split[0];
				$date = $time_date_split[1];
			}
				
			// Check Date Format
			$date_split = explode("/", $date); // Must be separated by / slash if in dd/mm/yyyy format otherwise is yyyymmdd
			
			if (($date_split[0] == $date) && ($date_split[0] != "") && (is_numeric($date_split[0]))) { // Date is in yyyymmdd format
				
				$return['8digit'] = $date;
				$return['year'] = round($date / 10000);
				$return['year_short'] = $return['year'] - 2000;
				$return['month'] = round(($date / 100) - ($return['year'] * 100));
				$return['day'] = $date - ($return['year'] * 10000) - ($return['month'] * 100); 
				$return["date_supplied"] = true;
						
			} elseif (count($date_split) == 3) { // Date is in [d]/[m]/[yy] format
		
				$return['year'] = $date_split[2] * 1;
				if ($return['year'] < 100) {
					$return['year_short'] = $return['year'];
					$return['year'] += 2000;
				} else {
					$return['year_short'] = $return['year'] - 2000;
				}
				$return['month'] = $date_split[1] * 1;
				$return['day'] = $date_split[0] * 1; 
				$return["date_supplied"] = true;
				
			} elseif ($date == "") { // No Date supplied
				
				$return["date_supplied"] = false;
				return $return;
			
			} else { // Data was not supplied in a recognisable format
			
				$return["error"] = true;
				return $return;
			}
		
			// Set Timestamp & Confirm 8digit set
			$return["timestamp"] = strtotime(trim($time . " " . $return['month'] . "/" . $return['day'] . "/" . $return['year'])); // strtotime presumes American m/d/y formatting. trim will strip off space at the start if time is empty
			if (($return['8digit'] == "") || ($return['8digit'] == null)) { // Date was not supplied in an 8digit format
				$return['8digit'] = $return['year'] . date("m", $return['timestamp']) . date("d", $return['timestamp']);
			}
		}
		
		// Check Time (actioned after Date so that we can use the timestamp with the date function)
		if ($time !== "") {
			$return["hours"] = date("g", $return['timestamp']);
			$return["minutes"] = date("i", $return['timestamp']);
			$return["ampm"] = date("a", $return['timestamp']); // am or pm
			$return["time_supplied"] = true;		
		}
		
		// Set up month and day names
		$return["month_name"] = date("F", $return['timestamp']);
		$return["day_name"] = date("l", $return['timestamp']);
		$return["day_2digits"] = date("d", $return['timestamp']); // Required for Events date display
		
		// Return
		return $return;	
	}
	

	public function dateSubtractor($initalValue, $subtractor) { // Inputs in number of days. $initial value is 8digit format. $subtractor could be eight digit or number of days (if value is greater than 19000101, then is treated as 8digit automatically)
		
		// Set up initial value as a date object
		$date = DateTime::createFromFormat('Ymd', $initalValue); // see http://stackoverflow.com/questions/5871963/php-strtotime-function-that-accepts-a-format
		
		if ($subtractor > 19000101) { // Subtract one date from another
			
			$subtractorDate = DateTime::createFromFormat('Ymd', $subtractor); // see http://stackoverflow.com/questions/5871963/php-strtotime-function-that-accepts-a-format
			
			// Return result in number of days
			$resultTimestamp = $date->getTimestamp() - $subtractorDate->getTimestamp();
			$result = ceil($resultTimestamp / $this->daySeconds); // Rounds up result to number of days
			return $result;
			
		} else { // Subtract a number of days
			
			$subtractorInSeconds = $subtractor * $this->daySeconds;
			
			// Return result in 8 digit format
			$resultTimestamp = $date->getTimestamp() - $subtractorInSeconds;
			$result = date("Ymd", $resultTimestamp);
			return $result;	
		}
	}


	public function dateAdder($initalValue, $adder) { // Inputs in number of days. $initial value is 8digit format. $adder is number of days
		
		// Set up initial value as a date object
		$date = DateTime::createFromFormat('Ymd', $initalValue); // see http://stackoverflow.com/questions/5871963/php-strtotime-function-that-accepts-a-format
		
		// Add a number of days
		$adderInSeconds = $adder * $this->daySeconds;
		
		// Return result in 8 digit format
		$resultTimestamp = $date->getTimestamp() + $adderInSeconds;
		$result = date("Ymd", $resultTimestamp);
		return $result;	
	}


	public function getDayNumber($eightDigit = "") {
		if ($eightDigit == "")
			$eightDigit = $this->today["8digit"];
		if ($eightDigit < $this->startDate["8digit"]) { // Date is before the Calendar
			$result = -1;
		} elseif ($eightDigit > $this->endDate["8digit"]) { // Date is after the Calendar
			$result = -2;
		} else { // Date is during the Calendar
			$result = $this->dateSubtractor($eightDigit, ($this->startDate["8digit"] -1)); // -1 so starts from day 0, to get an absolute day number - not how many days after day 1.
				// $this->tsp("Result: " . $result . "<br />8digit: " . $eightDigit . "<br />StartDate: " . $this->startDate["8digit"]);
		}
		return $result;
	}
	
	// Returns all start and end dates of each week in various formats
	public function getWeekDates() {
		$lentDayAbsolute = $this->todayDayNumber;
		$maxNumberOfWeeks = ceil($this->numberOfDays / 7);
		$calendarWeeks = array('is_lent' => false, 'lent_year' => date("Y",$this->startDate["timestamp"]), 'visible_week' => 0); // No need to take year into account any further as Easter will never cross the year boundary
		// Get week timestamps & textual details
		for ($week = 1; $week < $maxNumberOfWeeks+1; $week++) {
			// Timestamps
			if ($week == 1) {
				$calendarWeeks["week" . $week . "start_timestamp"] = $this->startDate["timestamp"];
			} else {
				$calendarWeeks["week" . $week . "start_timestamp"] = $this->startDate["timestamp"] + (($week - 1) * (7 * $this->daySeconds)) + $this->overcomeDaylightSavingIssuesBySeconds; // Adding two hours - helps with any daylight saving issues
			}
			$calendarWeeks["week" . $week . "end_timestamp"] = $this->startDate["timestamp"] + ($week * (7 * $this->daySeconds)) - $this->daySeconds + $this->overcomeDaylightSavingIssuesBySeconds; // Note that this is 6 days after the week start - 7 days is the beginning of the next week. Adding two hours
			
			if ($calendarWeeks["week" . $week . "end_timestamp"] > $this->pentecostSunday["timestamp"]) {
				$calendarWeeks["week" . $week . "end_timestamp"] = $this->pentecostSunday["timestamp"] + $this->overcomeDaylightSavingIssuesBySeconds; // Adding two hours
			}
			
			// Textual details
			// Nice version
			$calendarWeeks["week" . $week . "start_nice"] = date("g:i.sa d/m/Y", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_nice"] = date("g:i.sa d/m/Y", $calendarWeeks["week" . $week . "end_timestamp"]);
			// 8 digit (eg. 20150108)
			$calendarWeeks["week" . $week . "start_8digit"] = date("Ymd", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_8digit"] = date("Ymd", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Date (eg. 8)
			$calendarWeeks["week" . $week . "start_date"] = date("j", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_date"] = date("j", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Day Name (eg. Thursday)
			$calendarWeeks["week" . $week . "start_day_name"] = date("l", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_day_name"] = date("l", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Month (eg. 1)
			$calendarWeeks["week" . $week . "start_month"] = date("n", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_month"] = date("n", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Month Name (eg. January)
			$calendarWeeks["week" . $week . "start_month_name"] = date("F", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_month_name"] = date("F", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Short Month Name (eg. Jan)
			$calendarWeeks["week" . $week . "start_month_name_short"] = date("M", $calendarWeeks["week" . $week . "start_timestamp"]);
			$calendarWeeks["week" . $week . "end_month_name_short"] = date("M", $calendarWeeks["week" . $week . "end_timestamp"]);
			// Set up Display string with date and month
			if ($calendarWeeks["week" . $week . "start_month"] !== $calendarWeeks["week" . $week . "end_month"]) {
				$calendarWeeks['week' . $week . 'display'] = $calendarWeeks['week' . $week . 'start_date'] . " " . $calendarWeeks['week' . $week . 'start_month_name'] . " - " . $calendarWeeks['week' . $week . 'end_date'] . " " . $calendarWeeks['week' . $week . 'end_month_name'];
			} else {
				$calendarWeeks['week' . $week . 'display'] = $calendarWeeks['week' . $week . 'start_date'] . " - " . $calendarWeeks['week' . $week . 'end_date'] . " " . $calendarWeeks['week' . $week . 'start_month_name'];
			}
			// Add Year (no need to take into account as with month as Easter will never cross the year boundary)
			$calendarWeeks['week' . $week . 'display'] .= " " . $calendarWeeks["lent_year"];
			// Set up Short Display string with date and month (no Year added)
			if ($calendarWeeks["week" . $week . "start_month"] !== $calendarWeeks["week" . $week . "end_month"]) {
				$calendarWeeks['week' . $week . 'display_short'] = $calendarWeeks['week' . $week . 'start_date'] . " " . $calendarWeeks['week' . $week . 'start_month_name_short'] . " - " . $calendarWeeks['week' . $week . 'end_date'] . " " . $calendarWeeks['week' . $week . 'end_month_name_short'];
			} else {
				$calendarWeeks['week' . $week . 'display_short'] = $calendarWeeks['week' . $week . 'start_date'] . " - " . $calendarWeeks['week' . $week . 'end_date'] . " " . $calendarWeeks['week' . $week . 'start_month_name_short'];
			}
		}
		// Setup this week (including text string)
		if ($lentDayAbsolute > -1) { // Is a day during lent (-2 is before Lent, -1 is after Lent)
		
			$calendarWeeks['is_lent'] = true;
			$calendarWeeks['this_week'] = floor(($lentDayAbsolute + 1) / 7) + 1;
			$calendarWeeks['this_week_display'] = $calendarWeeks['week' . $calendarWeeks['this_week'] . 'display'];
			
		} else {
			$calendarWeeks['this_week'] = $lentDayAbsolute; // Sets it to -2 for before Lent or -1 for after Lent	
		}
	
		// Set minimum visible week (if any)
		if ($this->testMode == false) { // Set visibility
			
			$calendarWeeks["visible_week"] = floor(($this->today["timestamp"] - ($this->startDate["timestamp"] - (2 * $this->daySeconds) - ($this->visibleDaysInAdvance * $this->daySeconds))) / ($this->daySeconds * 7)) + 1; // Current Week
			$calendarWeeks["visible_week"] = floor(($this->today["timestamp"] - ($this->startDate["timestamp"] - ($this->visibleDaysInAdvance * $this->daySeconds))) / ($this->daySeconds * 7)) + 1; // Current Week
			$differenceTimestamp = $this->today["timestamp"] - ($this->startDate["timestamp"] - (2 * $this->daySeconds));
			/*$relativeTimeDifference = $differenceTimestamp % ($this->daySeconds * 7); // Relative time difference in days of the week
			if ($relativeTimeDifference > (($this->daySeconds * 4) - (180 * 60))) { // If week day is greater than 9pm Thursday, make next week visible also
				$calendarWeeks["visible_week"]++;
			}*/
			$calendarWeeks["visible_week"] = set_thresholds($calendarWeeks["visible_week"], 0, $maxNumberOfWeeks);
			
		} elseif (($this->testMode == true) && ($this->is_staff() == true)) { // Test mode active, display all weeks
			$calendarWeeks["visible_week"] = $maxNumberOfWeeks;
		} elseif ($this->testMode == true) { // Staff not logged in, reset test mode
			$this->testMode = false;
		}
				
		// Return
		return $calendarWeeks;
	}
	
	public function getDayNumericValue($calendarAbsoluteDayNumber) { // Returns 1 (for Monday) through 7 (for Sunday)
		
		// Get Timestamp for $calendarAbsoluteDayNumber
		$timestamp = $this->startDate["timestamp"] + (($calendarAbsoluteDayNumber-1) * $this->daySeconds);
		$dayNumeric = date("N", $timestamp);
		
		return $dayNumeric;
	}

	// Get information about the day from the Lent Number (NB: only set up to return information that is required at this stage - add more as required)
	public function get_day_info($lentDayAbsolute) {
		$lentDay = array('lent_day_absolute' => $lentDayAbsolute);
		// Get Timestamp for Lent Day
		$lentDay['timestamp'] = $this->startDate["timestamp"] + (($lentDayAbsolute - 1) * $this->daySeconds);
		// Get Information
		$lentDay['day_name'] = date('l', $lentDay['timestamp']);
		$lentDay['short_day_name'] = date('D', $lentDay['timestamp']);
		$lentDay['date'] = date('j', $lentDay['timestamp']);
		$lentDay['month'] = date('F', $lentDay['timestamp']);
		$lentDay['short_month'] = date('M', $lentDay['timestamp']);
		$lentDay['year'] = date('Y', $lentDay['timestamp']);
		// Return
		return $lentDay;
	}

	// Get Lent Name Prefix by day
	public function get_name_prefix($lentDayAbsolute) {
		$lentDayAbsolute = (int)$lentDayAbsolute;
		if (in_array($lentDayAbsolute, $this->specialDayNumbers)) {
			return $this->specialDaysSorted[$lentDayAbsolute]["name"];
		} else {
			return '&nbsp;';
		}
	}
	

	// Is anyone from Two Sparrows Logged in? For Data Testing
	public function tsp($data = "") {
		if (is_user_logged_in()) {
			global $current_user;
			get_currentuserinfo();
			if (($current_user->user_email == "tim@twosparrows.co.nz") && (($current_user->user_firstname == "Tim") || (($current_user->user_firstname == "Two") && ($current_user->user_lastname == "Sparrows"))) || ($current_user->user_email == "*@twosparrows.co.nz") && ($current_user->user_firstname == "*")) {
				if ($data != "") {
					echo '<p style="color: #f00;">';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</p>';
				}
				return true;
			}
		}
		return false;
	}
	
	// Is staff Logged in? For Data Testing
	public function is_staff($data="") { // Same as tsp() but for all administrators. Displays in a more website friendly format.
		if (is_user_logged_in()) {
			if (current_user_can('manage_options')) {
				if ($data != "") {
					echo '<p><strong><em>';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</em></strong></p>';
				}
				return true;
			}
		}
		return false;
	}

}
$resurrection = new calendar;


// If is in the Lent Challenge category (or sub category)
function is_lent($all = true) {
	$cat_names = array("introduction", "week1", "week2", "week3", "week4", "week5", "week6", "week7", "week8"); // Category slugs
	if ($all == true) { // $all enables checking for just a week subcategory
		$cat_names .= "lent";
	}
	if (is_category($cat_names)) { // We can search for multiple categories by slug as well
		return true;
	} else { // NB: Will probably return false for a single post within the Lent category at this stage
		return false;
	}
}


// Extracts the Lent Week or Day number from the category title (still required for category titles)
function lent_lent_number_extract($category_title) {
	$category_title = str_replace("&#8211;", "", $category_title);
	$category_title = filter_var($category_title, FILTER_SANITIZE_NUMBER_INT);
	return $category_title;
}


// Convert a digit to the word
function lent_digit_to_word($digit) {
	switch($digit) {
	    case 0: return "Zero";
	    case 1: return "One";
	    case 2: return "Two";
	    case 3: return "Three";
	    case 4: return "Four";
	    case 5: return "Five";
	    case 6: return "Six";
	    case 7: return "Seven";
	    case 8: return "Eight";
	    case 9: return "Nine";
	}
	return false; // Fallback
}

// Get number of Lent Day
function lent_get_lent_day($startTimestamp, $todayTimestamp) { // NB: Takes a timestamp input, but returns output in days. Outputs: -2 (before Lent), -1 (after Lent), 0 (Rest day during Lent) or Lent day number.
	global $daySeconds, $resurrection;
	$lent = array("before" => false, "after" => false);
	if ($startTimestamp > $todayTimestamp) { // Today is before Lent
		$lent["day_absolute"] = -2;
		$lent["before"] = true;
	} elseif ($todayTimestamp > $startTimestamp + (47 * $daySeconds)) { // Today is after Easter Sunday (more than 47 days after Ash Wednesday)
		$lent["day_absolute"] = -1;
		$lent["after"] = true;
	} else { // During Lent
		$lent["day_absolute"] = floor(($todayTimestamp - $startTimestamp) / $this->daySeconds) + 1;
		$lent["week"] = floor(($lent["day_absolute"] + 1) / 7) + 1; 
		$lent["week_word"] = lent_digit_to_word($lent["week"]);
		
		// Convert $lent["day_absolute"] to take into account the Sabbath rest days
		if ($lent["day_absolute"] == ($lent["week"] * 7) - 2) { // Sabbath
			$lent["sabbath"] = true;
			$lent["day"] = 0;
		} else {
			$lent["sabbath"] = false;	
			$lent["day"] = $lent["day_absolute"] - ($lent["week"] - 1);
		}		
	}
	return $lent;
}


// Get the absolute day number of a Lent Day (including rest days)
function lent_get_lent_day_absolute($lentDay, $lentWeek, $startTimestamp, $title) {
	if (substr(strtolower(trim($title)), 0, 4 ) == "rest") { // Rest Days
		$lentDayAbsolute = (($lentWeek - 1) * 7) + 5;
	} else { // Normal Lent Days
		$lentDayAbsolute = $lentDay + ($lentWeek - 1);
	}
	// Return
	return $lentDayAbsolute;
}
/*function lent_get_lent_day_absolute($lentDay, $lentWeek, $startTimestamp, $title) {
	if (substr(strtolower(trim($title)), 0, 4 ) == "rest") { // Rest Days
		$lentDayAbsolute = (($lentWeek - 1) * 7) + 5;
	} else { // Normal Lent Days
		$lentDayAbsolute = $lentDay + ($lentWeek - 1);
	}
	// Return
	return $lentDayAbsolute;
}
*/



// Change the category list order on Lent pages so that days display in the order desired.
function lent_lent_change_category_list_order( $query ) { // see https://wordpress.org/support/topic/sort-posts-in-ascending-order-for-archive-listing-only
	// Check if the query is for an archive
	if($query->is_archive)
		if (is_category( array("lent", "week1", "week2", "week3", "week4", "week5", "week6", "week7", "week8" )))
		// Query was for archive, then set order
		$query->set( 'order' , 'asc' ); // Ordered by time published
		// $query->set( 'orderby' , 'title' );
	// Return the query (else there's no more query, oops!)
	return $query;
}
// Runs before the posts are fetched
add_filter( 'pre_get_posts' , 'lent_lent_change_category_list_order' );


function lent_get_lent_week($lentDayAbsolute) {
	$lentWeek = floor(($lentDayAbsolute / 7) + 1);
	return $lentWeek;
}


/* DATA TESTING */

// Is anyone from Two Sparrows Logged in? For Data Testing
if (!(function_exists("tsp"))) {
	function tsp($data = "") {
		if (is_user_logged_in()) {
			global $current_user;
			get_currentuserinfo();
			if (($current_user->user_email == "tim@twosparrows.co.nz") && (($current_user->user_firstname == "Tim") || ($current_user->user_firstname == "Two")) || ($current_user->user_email == "*@twosparrows.co.nz") && ($current_user->user_firstname == "*")) {
				if ($data != "") {
					echo '<p style="color: #f00;">';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</p>';
				}
				return true;
			}
		}
		return false;
	}
}

// Is staff Logged in? For Data Testing
if (!(function_exists("is_staff"))) {
	function is_staff($data="") { // Same as is_tim() but for all administrators. Displays in a more website friendly format.
		if (is_user_logged_in()) {
			if (current_user_can('manage_options')) {
				if ($data != "") {
					echo '<p><strong><em>';
					if ((is_array($data)) || (is_object($data))) {
						print_r($data);
					} else {
						echo $data;
					}
					echo '</em></strong></p>';
				}
				return true;
			}
		}
		return false;
	}
}

?>