<?php 
	
	// $x =1; 
// Today Set up and Test Variables
/*$today = resurrection_date();*/
$setdate = false; // Required for the sidebar settings when in test mode
if (is_staff()) {
	if (isset($_GET['setdate'])) {
		$today = resurrection_date($_GET['setdate']); // setdate must be supplied in 8digit format
		$setdate = $today; // Required for the sidebar settings when in test mode
	}
}

// Manual Variable Setup
// Global Variables
if (substr(get_bloginfo("url"), 0, 22) === "http://localhost:8888/") {
	// Offline
	// $resCalCatID = 6; // Offline
	$homePageID = 2; // Offline
	$getURLCombinator = "&"; // Offline - used for testing
} else {
	// Online
	// $resCalCatID = 193; // Online
	$homePageID = 2; // Online, also set in inc/nav.php (globals not used so that nav.php can use this on any page)
	$getURLCombinator = "?"; // Online - used for testing
}

// Automatic Variable Setup
global $resurrection, $easterSunday;

$resCalDay = $resurrection->todayDayNumber; // Today's Lent Day number
$resCalWeeks = $resurrection->getWeekDates();
	/*tsp(date("g:ia D j F, Y - e"));
	tsp($resCalDay);
	tsp($resCalWeeks);*/

get_header(); ?>

	<?php if ((is_staff()) && ($setdate != false)) {
		is_staff("The date is currently being overridden by an Administrator and set to " . date('l j F Y', $setdate['timestamp']) . ".");
	}

	$resCalWeek = lent_lent_number_extract(single_cat_title('', false));
		// tsp($today); ?>

	<?php if ($resCalWeeks["visible_week"] >= $resCalWeek) { // Item is visible (is during or after lent (-1))

		if (have_posts()) : while (have_posts()) : the_post(); ?>
	
			<?php
				
			/*if ($x === 1) {

					$post_id = $post->ID;
					tsp('Post ID: ' . $post_id);
				    tsp("Old Return:");
			        tsp(get_attached_file(get_post_thumbnail_id($post_id)));
					tsp("New Return:");
			        // tsp(tsp_get_image_path($post_id));
					$image_id = get_post_thumbnail_id($post_id);
					tsp("Image ID: " . $image_id);
					$instagram_image = wp_get_attachment_image_src($image_id, $image_size);
			        tsp($instagram_image[0]);
			        // tsp("XX");
			
			   	$x = 1000;
			}*/


				
			// Set up Resurrection Calendar Day information
			$resCalDay = lent_lent_number_extract(get_the_title());
			$resCalDayAbsolute = $resCalDay; // lent_get_lent_day_absolute($resCalDay, $resCalWeek, $resurrection->startDate['timestamp'], get_the_title());
			//if (substr(strtolower(trim(get_the_title())), 0, 4 ) = "rest") {
			$resCalDayInfo = $resurrection->get_day_info($resCalDay);
				// tsp("lentDay: " . $resCalDay . "<br />lentWeek: " . $resCalWeek . "<br />lentCatTitle: " . single_cat_title('', false) . "<br />lentDayAbsolute: " . $resCalDayAbsolute . "<br />lentDayInfo: ");
				// tsp($resCalDayInfo);
			$resCalImageWidth = 600;
			$resCalImageSize = "single";
			$resCalSpecialSize = "";
			/*if ($resCalDayInfo['day_name'] == "Saturday" || $resCalDayInfo['day_name'] == "Sunday") {
				if (($resCalWeek == 8) && ($resCalDay == 57)) {
					// Triple this day instead
					$resCalImageWidth = 600;
					$resCalImageSize = "lent-day-triple";								
				} else {
					$resCalImageWidth = 600;
					$resCalImageSize = "lent-day-double";
				}
			}*/
			if ($resCalDayInfo['day_name'] == "Monday" && $resCalWeek > 1) {
				$resCalSpecialSize = "lent-day-single-sm"; // Half the width on Monday on SM size
			}
			$resCalNamePrefix = $resurrection->get_name_prefix($resCalDayAbsolute);
			// $resCalWeek = lent_get_lent_week($resCalDayAbsolute);
			
			?>
			
			<div class="lent-day-surround lent-<?php echo strtolower($resCalDayInfo["day_name"]); 
			if ($resCalImageSize == "lent-day-triple") {
				echo " lent-day-triple";
			} elseif ($resCalImageSize == "lent-day-double") {
				echo " lent-day-double";
			}
			if ($resCalSpecialSize == "lent-day-single-sm") {
				echo " lent-day-single-sm";
			} ?>">
				<div class="lent-day">
					<div class="lent-day-image">
						<?php // Get the thumbnail
						if (has_post_thumbnail()) {
							
							$resCalImageID = get_post_thumbnail_id($post->ID);
							$resCalImage = wp_get_attachment_metadata($resCalImageID);
							$resCalImageSrc = wp_get_attachment_image_src($resCalImageID, $resCalImageSize);
							$resCalImageRetinaSrc = wp_get_attachment_image_src($resCalImageID, $resCalImageSize . "-retina"); ?>
						
							<img src="<?php echo $resCalImageSrc[0]; ?>" data-retina="<?php echo $resCalImageRetinaSrc[0]; ?>" width="<?php echo $resCalImageWidth; ?>" height="300" title="<?php the_title(); ?>" />

						<?php } else { ?>
							<div class="lent-no-bg<?php /* width-<?php echo $resCalImageWidth; */ ?>"></div>
						<?php } ?>
						
					</div>

					<?php if (!in_category(array('uncategorised', 'uncategorised', 1))) { ?>
						
						<div class="lent-day-overlay"></div>
						
						<div class="lent-day-title"><?php // tsp($resCalDayInfo); ?>
							<h4><?php echo $resCalNamePrefix; ?></h4>
							<h3><?php echo $resCalDayInfo['short_day_name']; ?></h3>
							<h4><span class="numbers"><?php echo $resCalDayInfo['date']; ?></span> <?php echo $resCalDayInfo['short_month']; ?></h4>
							<?php /* <h3><?php // echo $resCalDayInfo['short_day_name']; // the_title(); ?></h3> */ ?>
							<?php /* if (($resCalDayInfo['day_name'] == "Sunday") && ($resCalWeek > 0) && ($resCalWeek < 7)) { ?>
								<h5>Lent <span class="numbers"><?php echo $resCalWeek; ?></span></h5>
							<?php } */ ?>
						</div>

					<?php } ?>
					
					<?php /*<div class="lent-day-date">
						<h4><span class="numbers"><?php echo $resCalDayInfo['date']; ?></span> <?php echo $resCalDayInfo['short_month']; ?></h4>
					</div> */ ?>

					<div class="lent-day-text">
						<div class="entry">
							<?php /* <h3><?php if ($resCalDayInfo['day_name'] == "Sunday") {
								// echo "Rest - Week";
								echo $resCalDayInfo['day_name'] . " " . $resCalDayInfo['date'] . " " . $resCalDayInfo['month']; 
							} else {
								echo get_the_title() . ": " . $resCalDayInfo['day_name'] . " " . $resCalDayInfo['date'] . " " . $resCalDayInfo['month']; 
							}?></h3> */ ?>
							<?php the_content(); ?>
						</div>
					</div>
					
					<?php /* <div class="clear"></div> */ ?>
				</div><?php // End lent-day ?>
			</div><?php // End lent-day-surround ?>


			<?php /* <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
				<h2><?php the_title(); ?></h2>
				
				<div class="entry">
					
					<?php the_content(); ?>
	
				</div>
				
			</div> */ ?>
	
		<?php endwhile; endif;

	} else { // Lent is not visible ?>
	
		<div class="post indent">
			<h2>Nearly there...</h2>
			<?php tsp($easterSunday); ?>
			<p>Sorry this item for Lent is not yet visible. It will be viewable from <?php echo date('l j F Y', ($resCalWeeks["week" . $resCalWeek . "start_timestamp"] - ($resurrection->daySeconds * $resurrection->visibleDaysInAdvance))); // (one week in advance) ?> (<?php echo $resurrection->visibleDaysInAdvanceDescription; ?> in advance).</p>
		</div>
		
	<?php }
	
get_footer(); ?>