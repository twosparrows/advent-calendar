<?php


/*
	NOTES ABOUT THIS SITE:
	
	- This site is built on the framework of the Lent Calendar, and then the Resurrection Calendar - hence there are still references to both throughout
	- The main class is created in inc/resurrection.php, which sets up all the dates etc
	- Because of the underlying frameworks from the Lent and Resurrection Calendars, the start date in the Resurrection class is known as $easterSunday, and the end date is known as $pentecostSunday
	- All dates are automatically calculated from the first Sunday in Advent. These dates are set up in the database until 2030 (see top of inc/resurrection.php file)
*/



// This template is used for the Easter category (and subcategories - via a function in functions.php) only

// NB: Lent ALWAYS begins on a Wednesday - Ash Wednesday - which is 46 days before Easter Sunday (from http://catholicism.about.com/od/lent/f/Lent_Start.htm)

// The Resurrection Calendar is calculated from Easter Sunday

// Today Set up and Test Variables
$setdate = false; // Required for the sidebar settings when in test mode
if (is_staff()) {
	if (isset($_GET['setdate'])) {
		$today = resurrection_date($_GET['setdate']); // setdate must be supplied in 8digit format
		$setdate = $today; // Required for the sidebar settings when in test mode
	}
}

// Manual Variable Setup
// Global Variables
if (substr(get_bloginfo("url"), 0, 22) === "http://localhost:8888/") {
	// Offline
	// $resCalCatID = 6; // Offline
	$homePageID = 2; // Offline
	$getURLCombinator = "&"; // Offline - used for testing
} else {
	// Online
	// $resCalCatID = 193; // Online
	$homePageID = 2; // Online, also set in inc/nav.php (globals not used so that nav.php can use this on any page)
	$getURLCombinator = "?"; // Online - used for testing
}

// Automatic Variable Setup
global $resurrection, $easterSunday;

$resCalDay = $resurrection->todayDayNumber; // Today's Day number
$resCalWeeks = $resurrection->getWeekDates();

get_header(); ?>

	<?php if ((is_staff()) && ($setdate != false)) {
		is_staff("The date is currently being overridden by an Administrator and set to " . date('l j F Y', $setdate['timestamp']) . ".");
	}

	$resCalWeek = lent_lent_number_extract(single_cat_title('', false));
		// tsp($resCalWeek); tsp($resCalWeeks);
	
	if (($resCalWeeks["visible_week"] >= $resCalWeek) || (is_staff())) { // Item is visible (is during or after calendar period (-1)) - Staff can always see the week.
		
		if ((is_staff()) && (($resCalWeeks["visible_week"] < $resCalWeek))) {
			is_staff("This week is currently only visible while logged in to WordPress. It will be publicly visible from " . date('l j F Y', ($resCalWeeks["week" . $resCalWeek . "start_timestamp"] - ($resurrection->daySeconds * $resurrection->visibleDaysInAdvance))) . " (" . $resurrection->visibleDaysInAdvanceDescription . " in advance).");
		}

		if (have_posts()) : ?>
	
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
						
				<div <?php post_class("lent-week-display") ?>><?php // .lent-week-display is used to limit the width on LG size screens ?>
					
					<?php while (have_posts()) : the_post();
						
						// Set up Resurrection Calendar Day information
						$resCalDay = lent_lent_number_extract(get_the_title());
						$resCalDayAbsolute = $resCalDay; 
						
						if ($resCalDayAbsolute <= $resurrection->numberOfDays) {
						
							$resCalDayInfo = $resurrection->get_day_info($resCalDay);
							$resCalImageWidth = 300;
							$resCalImageSize = "lent-day";
							$resCalSpecialSize = "";
							$resCalNamePrefix = $resurrection->get_name_prefix($resCalDayAbsolute);
							if (($resCalDayInfo['day_name'] == "Saturday") || ($resCalDayInfo['day_name'] == "Sunday") || (($resCalNamePrefix == "Christmas Day") && ($resCalDayInfo['day_name'] == "Wednesday"))) {
								$resCalImageWidth = 600;
								$resCalImageSize = "lent-day-double";
							}
							if (($resCalNamePrefix == "Christmas Day") && ($resCalDayInfo['day_name'] == "Sunday" || $resCalDayInfo['day_name'] == "Tuesday" || $resCalDayInfo['day_name'] == "Friday")) {
								// Triple this day instead
								$resCalImageWidth = 900;
								$resCalImageSize = "lent-day-triple";								
							}
							if ($resCalDayInfo['day_name'] == "Monday" && $resCalWeek > 1) {
								$resCalSpecialSize = "lent-day-single-sm"; // Half the width on Monday on SM size
							} ?>
							
							<div class="lent-day-surround lent-<?php echo strtolower($resCalDayInfo["day_name"]); 
							if ($resCalImageSize == "lent-day-triple") {
								echo " lent-day-triple";
							} elseif ($resCalImageSize == "lent-day-double") {
								echo " lent-day-double";
							}
							if ($resCalSpecialSize == "lent-day-single-sm") {
								echo " lent-day-single-sm";
							} ?>">
								<div class="lent-day">
									<div class="lent-day-image">
										<?php
											
										// Get the thumbnail
										if (has_post_thumbnail()) {
											
											$resCalImageID = get_post_thumbnail_id($post->ID);
											$resCalImage = wp_get_attachment_metadata($resCalImageID);
											$resCalImageSrc = wp_get_attachment_image_src($resCalImageID, $resCalImageSize);
											$resCalImageRetinaSrc = wp_get_attachment_image_src($resCalImageID, $resCalImageSize . "-retina"); ?>
										
											<img src="<?php echo $resCalImageSrc[0]; ?>" data-retina="<?php echo $resCalImageRetinaSrc[0]; ?>" width="<?php echo $resCalImageWidth; ?>" height="300" title="<?php the_title(); ?>" />
	
										<?php } else { ?>
											<div class="lent-no-bg"></div>
										<?php } ?>
										
									</div>
									
									<?php if (!in_category(array('uncategorised', 'uncategorised', 1))) { ?>
									
										<div class="lent-day-overlay"></div>
										
										<div class="lent-day-title"><?php // tsp($resCalDayInfo); ?>
											<h4><?php echo $resCalNamePrefix; ?></h4>
											<h3><?php echo $resCalDayInfo['short_day_name']; ?></h3>
											<h4><span class="numbers"><?php echo $resCalDayInfo['date']; ?></span> <?php echo $resCalDayInfo['short_month']; ?></h4>
										</div>
									
									<?php } ?>
	
									<div class="lent-day-text">
										<div class="entry">
											<?php the_content(); ?>
										</div>
									</div>
									
								</div><?php // End lent-day ?>
							</div><?php // End lent-day-surround
							
							
						} ?>
						
					<?php endwhile; ?>
					
				<div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>
					
			</div>
		
		<?php else : ?>
	
			<div class="post indent">
				<h2>Nothing found</h2>
				<p>Sorry, there was nothing found.</p>
			</div>
	
		<?php endif;
	
	} else { // Lent is not visible & Admin user not logged in ?>
	
		<div class="post indent">
			<h2>Nearly there...</h2>
			<p>Sorry this week is not yet visible. It will be viewable from <?php echo date('l j F Y', ($resCalWeeks["week" . $resCalWeek . "start_timestamp"] - ($resurrection->daySeconds * $resurrection->visibleDaysInAdvance))); // (three days in advance) ?> (<?php echo $resurrection->visibleDaysInAdvanceDescription; ?> in advance).</p>
		</div>
		
	<?php }

get_footer(); ?>